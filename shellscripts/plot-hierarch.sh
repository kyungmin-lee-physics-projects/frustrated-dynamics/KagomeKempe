#!/bin/bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
SCRIPT_DIR="$PROJECT_DIR/scripts"
parallel -j 8 julia "$SCRIPT_DIR/connectivity-hierarchy.jl" ::: "(2,0)x(0,2)" "(2,-2)x(2,4)" "(3,-3)x(3,6)" "(4,0)x(0,4)" "(5,0)x(0,5)" "(6,0)x(0,3)"
