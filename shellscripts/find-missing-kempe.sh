#!/bin/bash
# Find missing kempe graphs

scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a kempe_done_list=()
declare -a kempe_missing_list=()
for shape in $(cat all_shapes | awk '{print $1;}' )
do
    if [ -f "$scriptpath/../data/kempe-$shape.net" ] ; then
        kempe_done_list+=("$shape")
    else
        kempe_missing_list+=("$shape")
    fi
done

echo "${kempe_missing_list[@]}"
