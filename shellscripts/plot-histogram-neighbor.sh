#!/bin/bash
scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a shape_list=()
for fpath in "${scriptpath}/../data/"neighbor-histogram-6_*.csv
do
    fname="$(basename "$fpath")"
    f=${fname##neighbor-histogram-6_}
    shape+=(${f%%.csv})
done
echo "${shape[@]}"
parallel --lb -m -j 64 julia plot-histogram-neighbor.jl ::: ${shape[@]}


# parallel --lb -j 16 julia plot-histogram-neighbor.jl ::: "(1,-2)x(2,3)" "(2,-2)x(2,4)" "(2,0)x(0,2)" "(3,-3)x(3,6)" "(3,0)x(0,3)" "(3,0)x(1,2)" "(4,0)x(0,4)" "(5,0)x(0,5)" "(6,0)x(0,3)" "(6,0)x(0,6)"
