#!/bin/bash
# make kempe connectivity graph and histogram of loop length 6

scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a kempe_done_list=()
for shape in $(cat all_shapes | awk '{print $1;}' )
do
    [ -f "$scriptpath/../data/kempe-$shape.net" ] && kempe_done_list+=("$shape")
done

declare -a histogram_todo_list=()
for shape in "${kempe_done_list[@]}"
do
    if [ -f "$scriptpath/../data/neighbor-histogram-6_$shape.csv" ] ; then
        echo "$shape exists"
    else
        histogram_todo_list+=("$shape")
    fi
done

echo "${histogram_todo_list[@]}"

parallel --lb -j 34 julia "$scriptpath/process-histogram-neighbor.jl" ::: "${histogram_todo_list[@]}"
