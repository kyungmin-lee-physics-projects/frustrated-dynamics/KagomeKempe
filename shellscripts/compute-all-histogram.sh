#!/bin/bash
# make kempe connectivity graph and histogram of loop length 6

scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a kempe_todo_list=()
for shape in $(cat all_shapes | awk '{print $1;}' )
do
    if [ -f "$scriptpath/../data/kempe-$shape.net" ]; then
        continue
    else
        echo $shape
        kempe_todo_list+=("$shape")
    fi
done

parallel --lb -j 64 julia "$scriptpath/make-coloring-graph.jl" ::: "${kempe_todo_list[@]}"


declare -a kempe_done_list=()
for shape in $(cat all_shapes | awk '{print $1;}' )
do
    if [ -f "$scriptpath/../data/kempe-$shape.net" ]; then
        kempe_done_list+=("$shape")
    fi
done

declare -a histogram_todo_list=()
for shape in "${kempe_done_list[@]}"
do
    echo $shape
    if [ -f "$scriptpath/../data/neighbor-histogram-6_$shape.csv" ]; then
        continue
    else
        histogram_todo_list+=("$shape")
    fi
done

parallel --lb -j 34 julia "$scriptpath/process-histogram-neighbor.jl" ::: "${histogram_todo_list[@]}"
