#!/bin/bash
scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a shape_list=()
for fpath in "${scriptpath}/../data/"neighbor-histogram-6_*.csv
do
    fname="$(basename "$fpath")"
    f=${fname##neighbor-histogram-6_}
    shape+=(${f%%.csv})
done
#echo "${shape[@]}"
julia fit-histogram-neighbor.jl "${shape[@]}"
