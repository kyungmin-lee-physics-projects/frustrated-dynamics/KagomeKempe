module KagomeKempe

include("Preamble.jl")
include("Kagome.jl")
include("KagomeColoring.jl")

end # module
