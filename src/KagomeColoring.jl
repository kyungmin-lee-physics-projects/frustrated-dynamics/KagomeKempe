using LatticeTools
using LightGraphs



"""
    get_colorings(n_colors::T, graph::AbstractGraph) where {T<:Integer}

Enumerate all `n_colors` colorings of `graph`.
Only canonical colorings are counted.
"""
function get_colorings(n_colors::T, graph::AbstractGraph) where {T<:Integer}
    n_vertices = nv(graph)
    coloring_list = LightGraphs.Coloring{T}[]
    function color(vertex::Integer, coloring::Vector{Int})
        for c in 1:n_colors
            valid_coloring = true
            for neighbor_vertex in neighbors(graph, vertex)
                if coloring[neighbor_vertex] == c
                    valid_coloring = false
                    break
                end
            end
            valid_coloring || continue

            coloring[vertex] = c
            if vertex == n_vertices
                push!(coloring_list, LightGraphs.Coloring{T}(n_colors, copy(coloring)))
            else
                color(vertex+1, coloring)
            end
            coloring[vertex] = 0
        end
    end
    coloring = zeros(T, n_vertices)
    coloring[1] = 1
    color(2, coloring)
    return coloring_list
end


function get_colorings_parallel(n_colors::T, graph::AbstractGraph) where {T<:Integer}
    n_vertices = nv(graph)
    function color(vertex::Integer, coloring::Vector{Int}, coloring_list::Vector{LightGraphs.Coloring{T}})
        for c in 1:n_colors
            valid_coloring = true
            for neighbor_vertex in neighbors(graph, vertex)
                if coloring[neighbor_vertex] == c
                    valid_coloring = false
                    break
                end
            end
            valid_coloring || continue

            coloring[vertex] = c
            if vertex == n_vertices
                push!(coloring_list, LightGraphs.Coloring{T}(n_colors, copy(coloring)))
            else
                color(vertex+1, coloring, coloring_list)
            end
            coloring[vertex] = 0
        end
    end

    function coloring_bfs(initial_coloring_list::Vector{Vector{Int}}, start_vertex::Integer, end_vertex::Integer)
        prev_coloring_list = copy(initial_coloring_list)
        new_coloring_list = Vector{Int}[]
        for vertex in start_vertex:end_vertex
            empty!(new_coloring_list)
            for prev_coloring in prev_coloring_list
                for c in 1:n_colors
                    valid_coloring = true
                    for neighbor_vertex in neighbors(graph, vertex)
                        if prev_coloring[neighbor_vertex] == c
                            valid_coloring = false
                            break
                        end
                    end
                    valid_coloring || continue
                    coloring = copy(prev_coloring)
                    coloring[vertex] = c
                    push!(new_coloring_list, coloring)
                end
            end
            prev_coloring_list, new_coloring_list = new_coloring_list, prev_coloring_list
        end
        return prev_coloring_list
    end

    coloring = zeros(Int, n_vertices)
    coloring[1] = 1

    end_vertex = min(10, n_vertices)
    bfs_coloring_list = coloring_bfs([coloring], 2, end_vertex)
    if end_vertex == n_vertices
        return LightGraphs.Coloring{T}.(bfs_coloring_list)
    end
    
    lck = Threads.SpinLock()
    coloring_list = LightGraphs.Coloring{T}[]
    Threads.@threads for i in 1:length(bfs_coloring_list)
        coloring = bfs_coloring_list[i]
        threadlocal_coloring_list = LightGraphs.Coloring{T}[]
        color(end_vertex+1, coloring, threadlocal_coloring_list)
        lock(lck)
        append!(coloring_list, threadlocal_coloring_list)
        unlock(lck)
    end
    sort!(coloring_list, by=z -> z.colors)
    return coloring_list
end



"""
    canonize_coloring!(coloring::LightGraphs.Coloring)

Rotate colorings globally such that the site 1 has color 1.
"""
function canonize_coloring!(coloring::LightGraphs.Coloring)
    c0 = first(coloring.colors)
    for (i, c) in enumerate(coloring.colors)
        coloring.colors[i] = mod(c-c0, coloring.num_colors) + 1
    end
    return coloring
end


"""
    canonize_coloring(coloring::LightGraphs.Coloring{T}) where T

Rotate colorings globally such that the site 1 has color 1.
"""
function canonize_coloring(coloring::LightGraphs.Coloring{T}) where T
    c0 = first(coloring.colors)
    colors = Vector{T}(undef, length(coloring.colors))
    for (i, c) in enumerate(coloring.colors)
        colors[i] = mod(c-c0, coloring.num_colors) + 1
    end
    return LightGraphs.Coloring{T}(coloring.num_colors, colors)
end


function canonize_coloring!(num_colors::Integer, coloring::AbstractVector{T}) where T
    c0 = first(coloring)
    for (i, c) in enumerate(coloring)
        coloring[i] = mod(c-c0, num_colors) + 1
    end
    return coloring
end



"""
    is_canonical_coloring(coloring::LightGraphs.Coloring)

Test whether the coloring is canonical.
"""
function is_canonical_coloring(coloring::LightGraphs.Coloring)
    return isone(first(coloring.colors))
end


"""
    find_loops(graph::AbstractGraph, coloring::LightGraphs.Coloring)

Find all Kempe loops of coloring on graph.

# Returns
* `Vector{Vector{Int}}`: list of loops, where each loop is a list of sites in the loop.
"""
function find_loops(graph::AbstractGraph, coloring::LightGraphs.Coloring)
    # Every edge belongs to a single Kempe loop.
    edge_set = Set(collect(edges(graph)))
    loop_list = Vector{Int}[]
    while !isempty(edge_set)
        e1 = pop!(edge_set)
        v1, v2 = e1.src, e1.dst
        loop = Int[v1, v2]
        while true
            v3s = [v for v in neighbors(graph, v2)
                    if v != v1 && coloring.colors[v] == coloring.colors[v1]]
            @assert length(v3s) == 1
            v3 = first(v3s)
            delete!(edge_set, Edge(v2, v3))
            delete!(edge_set, Edge(v3, v2))
            if first(loop) == v3
                break
            else
                push!(loop, v3)
                v1 = v2
                v2 = v3
            end
        end
        push!(loop_list, loop)
    end
    return loop_list
end


"""
    canonize_loop(loop::AbstractVector{<:Integer})

Canonize a loop.
A canonical loop is the minimum representation; it starts at the site with the minumum
index, and the next site in the loop is the smaller of the two possibilities.
"""
function canonize_loop(loop::AbstractVector{<:Integer})
    len = length(loop)
    start = argmin(loop)
    if loop[mod(start, len)+1] > loop[mod(start-2, len)+1]
        dir = -1
    else
        dir = 1
    end
    new_loop = Vector{Int}(undef, len)
    j = start
    for i in 1:len
        new_loop[i] = loop[j]
        j = mod(j + dir - 1, len) + 1
    end
    return new_loop
end


"""
    is_valid_coloring(graph::AbstractGraph, coloring::LightGraphs.Coloring)

Test whether coloring is valid.
"""
function is_valid_coloring(graph::AbstractGraph, coloring::LightGraphs.Coloring)
    all(coloring.colors[e1.src] != coloring.colors[e1.dst] for e1 in edges(graph))
end


"""
    kempe_move!(coloring::LightGraphs.Coloring, loop::AbstractVector{<:Integer})

Make a Kempe move on a coloring.
"""
function kempe_move!(coloring::LightGraphs.Coloring, loop::AbstractVector{<:Integer})
    c1 = coloring.colors[loop[1]]
    c2 = coloring.colors[loop[2]]
    for i in loop
        if coloring.colors[i] == c1
            coloring.colors[i] = c2
        elseif coloring.colors[i] == c2
            coloring.colors[i] = c1
        else
            @show coloring, loop,
            @show coloring.colors[loop], c1, c2
            @assert false
        end
    end
    return coloring
end


"""
    kempe_move(coloring::LightGraphs.Coloring, loop::AbstractVector{<:Integer})

Make a Kempe move on a coloring.
"""
function kempe_move(coloring::LightGraphs.Coloring, loop::AbstractVector{<:Integer})
    new_coloring = deepcopy(coloring)
    kempe_move!(new_coloring, loop)
    return new_coloring
end


"""
    make_coloring_graph(lattice_graph::AbstractGraph{T}) where {T<:Integer}

Make a coloring graph on `lattice_graph`.
"""
function make_coloring_graph(lattice_graph::AbstractGraph{T}) where {T<:Integer}
    edge_set = Set(collect(edges(lattice_graph)))

    @info "Generating colorings"
    flush(stdout)
    all_colorings = get_colorings_parallel(Int8(3), lattice_graph)

    threadlocal_moves = [
        Tuple{Tuple{Int, Int}, @NamedTuple{loop_length::Int}}[]
        for i in 1:Threads.nthreads()
    ]

    @info "Generating moves"
    flush(stdout)
    Threads.@threads for i1 in 1:length(all_colorings)
        tid = Threads.threadid()
        c1 = all_colorings[i1]
        kempe_loops = canonize_loop.(find_loops(lattice_graph, c1))
        for loop in kempe_loops
            c2 = canonize_coloring(kempe_move(c1, loop))
            i2 = findfirst(x -> x.colors == c2.colors, all_colorings)
            @assert !isnothing(i2)
            push!(threadlocal_moves[tid], ((i1, i2), (loop_length=length(loop),)))
        end
    end
    all_moves = vcat(threadlocal_moves...)
    sort!(all_moves)

    return (all_colorings, all_moves)
end


function make_coloring_graph6(kagome, lattice_graph::AbstractGraph{T}) where {T<:Integer}
    @info "Generating colorings"
    flush(stdout)
    all_colorings = get_colorings_parallel(Int8(3), lattice_graph)

    threadlocal_moves = [
        Tuple{Tuple{Int, Int}, @NamedTuple{loop_length::Int}}[]
        for i in 1:Threads.nthreads()
    ]
    threadlocal_c2 = [
        zeros(Int8, nv(lattice_graph))
        for i in 1:Threads.nthreads()
    ]

    @info "Generating moves"
    flush(stdout)

    hexa_indices_list = [
        [i for (i, R) in hexa]
        for hexa in kagome.motifs.hexagons
    ]

    Threads.@threads for i1 in 1:length(all_colorings)
        tid = Threads.threadid()
        c1 = all_colorings[i1]
        c2 = threadlocal_c2[tid]
        for hexa in hexa_indices_list
            hc1 = c1.colors[hexa]
            if     hc1 == [1,2,1,2,1,2]
                hc2 = [2,1,2,1,2,1]
            elseif hc1 == [1,3,1,3,1,3]
                hc2 = [3,1,3,1,3,1]
            elseif hc1 == [2,1,2,1,2,1]
                hc2 = [1,2,1,2,1,2]
            elseif hc1 == [2,3,2,3,2,3]
                hc2 = [3,2,3,2,3,2]
            elseif hc1 == [3,1,3,1,3,1]
                hc2 = [1,3,1,3,1,3]
            elseif hc1 == [3,2,3,2,3,2]
                hc2 = [2,3,2,3,2,3]
            else
                continue
            end

            copy!(c2, c1.colors)
            c2[hexa] = hc2
            canonize_coloring!(c1.num_colors, c2)
            i2 = findfirst(x -> x.colors == c2, all_colorings)
            @assert !isnothing(i2)
            push!(threadlocal_moves[tid], ((i1, i2), (loop_length=6,)))
        end
    end
    all_moves = vcat(threadlocal_moves...)
    sort!(all_moves)

    return (all_colorings, all_moves)
end
