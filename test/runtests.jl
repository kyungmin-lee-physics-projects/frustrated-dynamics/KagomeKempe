using Test
using KagomeKempe
using LatticeTools
using LightGraphs


@testset "KagomeKempe" begin
    shape = [6 0; 2 5]
    kagome = KagomeKempe.make_kagome_lattice(shape);
    n_sites = numsite(kagome.lattice.supercell);
    lattice_graph = SimpleGraph(n_sites)
    for ((i, j), R, _) in kagome.nearest_neighbor_bonds
        add_edge!(lattice_graph, (i,j))
    end
    x = KagomeKempe.get_colorings_parallel(3, lattice_graph)
    y = KagomeKempe.get_colorings(3, lattice_graph)
    @show length(x), length(y)
    @show all(z -> z[1].colors == z[2].colors, zip(x,y))
    # @show Set([z.colors for z in x]) == Set([z.colors for z in y])
end