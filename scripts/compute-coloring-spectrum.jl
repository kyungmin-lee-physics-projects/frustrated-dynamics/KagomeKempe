using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

import KagomeKempe

using Random
using LinearAlgebra
using DataStructures

using Logging
using Printf
using Formatting
using ArgParse

using CodecXz
using MsgPack
using JSON
using BitIntegers

using LatticeTools
using QuantumHamiltonian
using Arpack

using LightGraphs

using PyCall
npl = pyimport("numpy.linalg")

function compute_spectrum_kempe(
    decay_length::Real,
    shape::AbstractMatrix{<:Integer},
)

    @mylogmsg "lattice shape: $shape"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    kagome = KagomeKempe.make_kagome_lattice(shape)

    n_sites = numsite(kagome.lattice.supercell)
    @mylogmsg "Number of sites: $n_sites"

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    coloring_site = Site([State("R"), State("G"), State("B")])

    hs = HilbertSpace([coloring_site for i in 1:n_sites])

    lattice_graph = SimpleGraph(n_sites)
    for ((i, j), R, _) in kagome.nearest_neighbor_bonds
        add_edge!(lattice_graph, (i,j))
    end

    # Generate colorings and moves
    (all_colorings, all_moves) = KagomeKempe.make_coloring_graph(lattice_graph)

    dim = length(all_colorings)
    hamiltonian = zeros(Float64, (dim, dim))
    for ((i, j), attr) in all_moves
        l = attr.loop_length - 6
        hamiltonian[i,j] += exp(-l)
    end
    @show maximum(abs.(hamiltonian - adjoint(hamiltonian)))
    (eigenvalues, eigenvectors) = npl.eigh(hamiltonian)

end


function parse_commandline()
    s = ArgParseSettings(
        description="""
        Generate a coloring graph file in Pajek .net format.

        The nodes are labeled by their colorings, and the edges are weighted by the Kempe
        loop length.
        """
    )
    @add_arg_table! s begin
        "shape"
            help = "Shape of the lattice, in format (n11,n12)x(n21,n22)"
            arg_type = String
            required = true
        "--out", "-o"
            help = "Name of the output file. Default is kempe-[shape].net"
            arg_type = String
            required = false
            default = nothing
    end
    return parse_args(s)
end


function parse_shape(shape_str::AbstractString)
    shape_pattern = r"\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)x\(\s*([-+]?\d+)\s*,\s*([-+]?\d+)\s*\)"
    m = match(shape_pattern, shape_str)
    if isnothing(m)
        throw(ArgumentError("shape should be in format (n11,n12)x(n21,n22)"))
    end
    n11, n12, n21, n22 = [parse(Int, x) for x in m.captures]
    return [n11 n21; n12 n22]
end

function main()
    parsed_args = parse_commandline()
    shape = parse_shape(parsed_args["shape"])
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"

    compute_spectrum_kempe(1.0, shape)



    #=
    output_filename = isnothing(parsed_args["out"]) ? "kempe-$shape_str.net" : parsed_args["out"]

    # Make Kagome lattice
    kagome = KagomeKempe.make_kagome_lattice(shape);
    n_sites = numsite(kagome.lattice.supercell);
    lattice_graph = SimpleGraph(n_sites)
    for ((i, j), R, _) in kagome.nearest_neighbor_bonds
        add_edge!(lattice_graph, (i,j))
    end

    # Generate colorings and moves
    (all_colorings, all_moves) = KagomeKempe.make_coloring_graph(lattice_graph)

    # Save colorings
    open(output_filename, "w") do fp
        println(fp, "*Vertices $(length(all_colorings))")
        for (i, c) in enumerate(all_colorings)
            println(fp, "$i \"$(join(string(x) for x in c.colors))\"")
        end
        println(fp, "*Edges :1 \"LoopLength\"")
        for ((r, c), attr) in all_moves
            if r < c
                println(fp, "$r $c $(attr.loop_length)")
            end
        end
        close(fp)
    end
    =#
end

main()
