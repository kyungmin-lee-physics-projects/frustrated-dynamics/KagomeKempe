using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using LightGraphs
using SimpleWeightedGraphs
using StatsBase

function loadweightednet(io::IO, gname::String = "graph", ::Type{T}=Float64) where {T}
    line = readline(io)
    # skip comments
    while startswith(line, "%")
        line = readline(io)
    end
    n = parse(Int, match(r"\d+", line).match)
    for ioline in eachline(io)
        line = ioline
        (occursin(r"^\*Arcs", line) || occursin(r"^\*Edges", line)) && break
    end
    if occursin(r"^\*Arcs", line)
        g = SimpleWeightedGraphs.SimpleWeightedDiGraph(n)
    else
        g = SimpleWeightedGraphs.SimpleWeightedGraph(n)
    end
    while occursin(r"^\*Arcs", line)
        for ioline in eachline(io)
            line = ioline
            # ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            ms = split(line)
            length(ms) < 3 && break
            add_edge!(g, parse(Int, ms[1]), parse(Int, ms[2]), parse(T, ms[3]))
        end
    end
    while occursin(r"^\*Edges", line) # add edges in both directions
        for ioline in eachline(io)
            line = ioline
            ms = split(line)
            #ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 3 && break
            i1, i2 = parse(Int, ms[1]), parse(Int, ms[2])
            w = parse(T, ms[3])
            add_edge!(g, i1, i2, w)
            add_edge!(g, i2, i1, w)
        end
    end
    return g
end


function simpleprocess(io::IO, gname::String = "graph", ::Type{T}=Float64) where {T}
    line = readline(io)
    # skip comments
    while startswith(line, "%")
        line = readline(io)
    end
    n = parse(Int, match(r"\d+", line).match)
    for ioline in eachline(io)
        line = ioline
        (occursin(r"^\*Arcs", line) || occursin(r"^\*Edges", line)) && break
    end
    g = Tuple{Int, Int, T}[]
    while occursin(r"^\*Arcs", line)
        for ioline in eachline(io)
            line = ioline
            # ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            ms = split(line)
            length(ms) < 3 && break
            push!(g, (parse(Int, ms[1]), parse(Int, ms[2]), parse(T, ms[3])))
        end
    end
    while occursin(r"^\*Edges", line) # add edges in both directions
        for ioline in eachline(io)
            line = ioline
            ms = split(line)
            #ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 3 && break
            i1, i2 = parse(Int, ms[1]), parse(Int, ms[2])
            w = parse(T, ms[3])
            if w == 6
                push!(g, (i1, i2, w))
            end
            # add_edge!(g, i1, i2, w)
            # add_edge!(g, i2, i1, w)
        end
    end
    return (n, g)
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
            nargs = '+'
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    for shape_str = parsed_args["shape"]
        try
            @info "$shape_str: Reading data"
            flush(stdout)
            if ispath(datadir("kempe6-$shape_str.lg"))
                @warn "File $(datadir("kempe6-$shape_str.lg")) exists. Skipping."
                continue
            end

            # graph = open(datadir("kempe-$shape_str.net"), "r") do io
            #     loadweightednet(io)
            # end

            # @info "$shape_str: Projecting to loop length 6"
            # flush(stdout)
            # graph6 = SimpleGraph(nv(graph))
            # for e1 in edges(graph)
            #     if e1.weight == 6
            #         add_edge!(graph6, e1.src, e1.dst)
            #     end
            # end

            (n, edgelist) = open(datadir("kempe-$shape_str.net"), "r") do io
                simpleprocess(io)
            end

            @info "$shape_str: Projecting to loop length 6"
            flush(stdout)
            graph6 = SimpleGraph(n)
            for (src, dst, weight) in edgelist
                if weight == 6
                    add_edge!(graph6, src, dst)
                end
            end

            @info "$shape_str: Saving kempe6"
            flush(stdout)
            savegraph(datadir("kempe6-$shape_str.lg"), graph6)
        catch e
            @error "Failed to process $shape_str"
            @error "Exception: $e"
            flush(stdout)
        end
    end
end

main()

