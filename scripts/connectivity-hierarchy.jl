# Make hierarchical connectivity graph

using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using LightGraphs
using GraphPlot
using Cairo, Compose

function read_graph(filename::AbstractString)
    open(filename, "r") do io
        return read_graph(io)
    end
end

function read_graph(io::IO)
    v, vertex_count_str = split(readline(io))
    if lowercase(v) != "*vertices"
        throw(ArgumentError("parse error at $v"))
    end
    vertex_count = parse(Int, vertex_count_str)

    vertex_list = Vector{String}(undef, vertex_count)

    for i in 1:vertex_count
        vertex_index_str, vertex_name = split(readline(io))
        if i != parse(Int, vertex_index_str)
            throw(ArgumentError("parse error at $vertex_index_str"))
        end
        vertex_list[i] = vertex_name
    end

    line = readline(io)
    if lowercase(split(line)[1]) != "*edges"
        throw(ArgumentError("parse error at $line"))
    end

    edge_list = Tuple{Int, Int, Int}[]
    while !eof(io)
        line = readline(io)
        if isempty(strip(line))
            break
        end
        (i, j, k) = let
            i, j, k = split(line)
            (parse(Int, i), parse(Int, j), parse(Int, k))
        end
        if !(1 <= i <= vertex_count && 1 <= j <= vertex_count)
            throw(ArgumentError("vertex out of bounds"))
        end
        push!(edge_list, (i,j,k))
    end

    return vertex_list, edge_list
end


function find_components(vertex_list, edge_list)

    loop_length_list = sort(unique(x[3] for x in edge_list))

    # at loop length 0, every vertex is a component
    components = Dict(0 => (
        component_list=[[x] for x in 1:length(vertex_list)],
        membership=collect(1:length(vertex_list)),
    ))

    prev_loop_length = 0
    for curr_loop_length in loop_length_list

        # components of previous are vertices of current
        prev_component_list = components[prev_loop_length].component_list
        prev_membership = components[prev_loop_length].membership

        curr_vertex_count = length(prev_component_list)

        # ---
        curr_graph = SimpleGraph(curr_vertex_count)
        for (i, j, k) in edge_list
            if k == curr_loop_length
                ic = prev_membership[i]
                jc = prev_membership[j]
                add_edge!(curr_graph, (ic, jc))
            end
        end
        compo_list = connected_components(curr_graph)
        # ---

        curr_membership = Vector{Int}(undef, length(vertex_list))
        fill!(curr_membership, -1)

        #for i in eachindex(vertex_list)
        for (icompo, compo) in enumerate(compo_list)
            for ic in compo
                for i in prev_component_list[ic]
                    curr_membership[i] = icompo
                end
            end
        end

        curr_component_list = [Int[] for i in compo_list]
        for (i, icompo) in enumerate(curr_membership)
            push!(curr_component_list[icompo], i)
        end

        components[curr_loop_length] = (
            component_list=curr_component_list,
            membership=curr_membership,
        )
        prev_loop_length = curr_loop_length
    end
    return components
end



function make_hierarch(components)

    all_node_list = Tuple{Int, Int}[]
    for loop_length in sort(collect(keys(components)))
        for i in 1:length(components[loop_length].component_list)
            push!(all_node_list, (loop_length, i))
        end
    end
    all_node_lookup = Dict(x => i for (i, x) in enumerate(all_node_list))
    all_edge_list = Tuple{Int, Int}[]

    prev_loop_length = 0
    prev = components[0]

    loop_length_list = sort(collect(keys(components)))
    for curr_loop_length in loop_length_list[2:end]

        curr = components[curr_loop_length]

        curr.component_list
        curr.membership

        # prev is leafy
        for (idx_prev_compo, prev_compo) in enumerate(prev.component_list)
            i = prev_compo[1]
            idx_curr_compo = curr.membership[i]

            ic = all_node_lookup[(curr_loop_length, idx_curr_compo)]
            ip = all_node_lookup[(prev_loop_length, idx_prev_compo)]
            push!(all_edge_list, (ic, ip))
        end

        prev_loop_length = curr_loop_length
        prev = curr
    end
    return all_node_list, all_edge_list
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    shape_str = parsed_args["shape"]

    vertex_list, edge_list = read_graph(datadir("kempe-$shape_str.net"))
    components = find_components(vertex_list, edge_list)

    node_list, edge_list = make_hierarch(components)

    edge_set = Set(edge_list)

    # trim
    dirty = true
    while dirty
        dirty = false
        for (i, j) in edge_set                      # from top to bottom
            if count(x -> x[1] == i, edge_set) == 1 # if lower edge is singular
                upper_edge_count = count(x -> x[2] == i, edge_set)
                if upper_edge_count == 1            # if upper edge is singular
                    for (k, i_) in edge_set
                        if i == i_
                            delete!(edge_set, (i, j))
                            delete!(edge_set, (k, i))
                            push!(edge_set, (k, j))
                            dirty = true
                            break
                        end
                        dirty && break
                    end
                elseif upper_edge_count == 0
                    delete!(edge_set, (i, j))
                    dirty = true
                    break
                else
                    println("Error: $i $j")
                    exit(1)
                end
            end
            dirty && break
        end
    end

    edge_list = collect(edge_set)

    open(datadir("hierarch-$shape_str.gv"), "w") do io
        println(io, "strict digraph hierarch {")
        println(io,
            """
            // graph [nodesep=0.01 ranksep=0.4 splines="line" labelloc="t" label="$shape_str" fontsize=12];
            // graph [nodesep=0.01 ranksep=1 splines="line" labelloc="t" label="$shape_str" fontsize=12];
            graph [nodesep=0.01 ranksep=10 splines="line" labelloc="t" label="$shape_str" fontsize=12];
            edge [arrowhead="none" penwidth="0.2"];
            """
        )
        length_list = sort(unique(x for (x, _) in node_list))

        # group nodes by length
        for l in length_list
            println(io, "subgraph g$l {")
            println(io, "label=\"$l\";")
            println(io, "rank=\"same\";")
            for (i, n) in enumerate(node_list)
                if all(x -> x[1] != i && x[2] != i, edge_list)
                    continue
                end
                if n[1] == l
                    println(io, "$i;")
                end
            end
            println(io, "}")
        end

        # node [shape=circle width=0.1 label=""];
        for (i, n) in enumerate(node_list)
            if all(x -> x[1] != i && x[2] != i, edge_list)
                continue
            end

            if n[1] == 0
                #println(io, "$i [shape=point width=0.01 label=\"$(n[1])\" fixedsize=true fontsize=8];")
                println(io, "$i [shape=point width=0.01 fixedsize=true];")
                # println(io, "$i [shape=circle width=0.2 label=\"\"];")
            else
                if count(x -> x[1] == i, edge_list) == 1
                    println(io, "$i [shape=point width=0.01 label=\"$(n[1])\" fixedsize=true fontsize=8];")
                else
                    #println(io, "$i [shape=oval height=0.25 width=0.35 label=\"$(n[1])\" fixedsize=true fontsize=8];")
                    println(io, "$i [shape=oval height=0.17 width=0.17 label=\"$(n[1])\" fixedsize=true fontsize=8];")
                end
                #println(io, "$i [shape=rect height=0.25 width=0.35 label=\"$(n[1])\" fixedsize=true fontsize=12];")
                # println(io, "$i [shape=none width=0 height=0 label=\"\" fixedsize=true margin=\"0,0\"];")
                # println(io, "$i [shape=circle width=0.2 label=\"\"];")
            end
        end

        for (i, j) in edge_list
            #if node_list[i][1] == 0 || node_list[j][1] == 0
            #else
                println(io, "$i -> $j;")
            #end
        end
        println(io, "}")
    end


    # run(`dot -Tpng -o $(plotsdir("hierarch-$shape_str.png")) $(datadir("hierarch-$shape_str.gv"))`)
    # run(`dot -Tsvg -o $(plotsdir("hierarch-$shape_str.svg")) $(datadir("hierarch-$shape_str.gv"))`)
    # run(`dot -Tpdf -o $(plotsdir("hierarch-$shape_str.pdf")) $(datadir("hierarch-$shape_str.gv"))`)

    run(`twopi -Tpng -o $(plotsdir("hierarch-$shape_str-twopi.png")) $(datadir("hierarch-$shape_str.gv"))`)
    run(`twopi -Tsvg -o $(plotsdir("hierarch-$shape_str-twopi.svg")) $(datadir("hierarch-$shape_str.gv"))`)
    run(`twopi -Tpdf -o $(plotsdir("hierarch-$shape_str-twopi.pdf")) $(datadir("hierarch-$shape_str.gv"))`)
end

main()
