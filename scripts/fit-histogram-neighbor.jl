using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using PyCall
using LsqFit
using DataFrames
using CSV
using LightGraphs

scipy = pyimport("scipy")
scipy_stats = pyimport("scipy.stats")

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
            nargs = '+'
    end
    return parse_args(s)
end

function parse_shape(shape_str::AbstractString)
    m = match(r"\(([-+]?\d+),([-+]?\d+)\)x\(([-+]?\d+),([-+]?\d+)\)", shape_str)
    isnothing(m) && return nothing
    s = parse.(Int, m.captures)
    return [ s[1]  s[3];
             s[2]  s[4] ]
end

function main()
    parsed_args = parse_commandline()

    println("shape,size,dimension,mean_degree,n_components")
    for shape_str = parsed_args["shape"]
        shape = parse_shape(shape_str)
        n = shape[1, 1] * shape[2,2] - shape[1,2] * shape[2,1]

        graph = loadgraph(datadir("kempe6-$(shape_str).lg"))
        nc = length(connected_components(graph))


        df = CSV.read(datadir("neighbor-histogram-6_$(shape_str).csv"), DataFrame)
        degree_list = df[:, :degree]
        frequency_list = df[:, :frequency]

        λ = sum(degree_list .* frequency_list) / sum(frequency_list)
        println("\"$shape_str\",$n,$(sum(frequency_list)),$λ,$nc")
    end
end

main()

