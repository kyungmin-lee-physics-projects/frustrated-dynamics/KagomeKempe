using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
import StatsBase
using LightGraphs

using LatticeTools

import KagomeKempe
using Logging



function parse_commandline()
    s = ArgParseSettings(
        description="""
        Generate a coloring graph file in Pajek .net format.

        The nodes are labeled by their colorings, and the edges are weighted by the Kempe
        loop length.
        """
    )
    @add_arg_table! s begin
        "shape"
            help = "Shape of the lattice, in format (n11,n12)x(n21,n22)"
            arg_type = String
            required = true
        "--out", "-o"
            help = "Name of the output file. Default is kempe-[shape].net"
            arg_type = String
            required = false
            default = nothing
    end
    return parse_args(s)
end



#shape = [2 2; -2 4]   # 36 = 12 * 3
#shape = [4 0; 0 4]    # 48 = 16 * 3
#shape = [3 3; -3 6]  # 81 = 27 * 3
#for (r, c, v) in zip(findnz(coloring_graph.weights)...)

function main()
    parsed_args = parse_commandline()

    logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=KagomeKempe.my_metafmt)
    global_logger(logger)

    shape = KagomeKempe.parse_shape(parsed_args["shape"])
    shape_str = "($(shape[1,1]),$(shape[2,1]))x($(shape[1,2]),$(shape[2,2]))"
    output_filename = isnothing(parsed_args["out"]) ? datadir("kempe-$shape_str.net") : parsed_args["out"]
    if ispath(output_filename)
        @error "File $output_filename exists. Skipping."
        exit(1)
    end

    # Make Kagome lattice
    kagome = KagomeKempe.make_kagome_lattice(shape);
    n_sites = numsite(kagome.lattice.supercell);
    lattice_graph = SimpleGraph(n_sites)
    for ((i, j), R, _) in kagome.nearest_neighbor_bonds
        add_edge!(lattice_graph, (i,j))
    end

    # Generate colorings and moves
    (all_colorings, all_moves) = KagomeKempe.make_coloring_graph(lattice_graph)

    @show length(all_colorings)
    @show length(all_moves)

    # Save colorings
    open(output_filename, "w") do fp
        println(fp, "*Vertices $(length(all_colorings))")
        for (i, c) in enumerate(all_colorings)
            println(fp, "$i \"$(join(string(x) for x in c.colors))\"")
        end
        println(fp, "*Edges :1 \"LoopLength\"")
        for ((r, c), attr) in all_moves
            if r < c
                println(fp, "$r $c $(attr.loop_length)")
            end
        end
        close(fp)
    end
end

main()
