using ArgParse
using LinearAlgebra
#using SparseArrays
using DataStructures
using MsgPack
using CodecXz
using PyCall
spl = pyimport("scipy.linalg")


function read_net(io::IO)
    tokens = split(readline(io))
    @assert tokens[1] == "*Vertices"
    nv = parse(Int, tokens[2])
    vertex_name_list = Vector{String}(undef, nv)
    for i in 1:nv
        tokens = split(readline(io))
        @assert parse(Int, tokens[1]) == i
        if tokens[2][1] == '"' && tokens[2][end] == '"'
            vertex_name_list[i] = tokens[2][2:end-1]
        else
            vertex_name_list[i] = tokens[2]
        end
    end

    tokens = split(readline(io))
    @assert tokens[1] == "*Edges"
    # TODO: parse rest

    edge_list = Pair{Int, Int}[]
    edge_prop_list = NamedTuple{(:loop_length,), Tuple{Int}}[]
    while !eof(io)
        line = strip(readline(io))
        isempty(line) && break
        tokens = split(line)
        i = parse(Int, tokens[1])
        j = parse(Int, tokens[2])
        @assert 1 <= i <= nv
        @assert 1 <= j <= nv
        @assert i != j
        loop_length = parse(Int, tokens[3])
        push!(edge_list, (i=>j))
        push!(edge_prop_list, (loop_length=loop_length,))
    end
    return (vertex_name_list, edge_list, edge_prop_list)
end


function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "file"
            arg_type = String
            required = true
        "-o"
            arg_type = String
            required = true
    end
    return parse_args(s)
end


function main()
    args = parse_commandline()
    (vertex_name_list, edge_list, edge_prop_list) = open(args["file"], "r") do io
        read_net(io)
    end
    @info "Read"

    vertex_name_lookup = Dict(name => idx for (idx, name) in enumerate(vertex_name_list))
    @info "Constructed lookup"

    hopping_hamiltonian_dense = let
        n = length(vertex_name_list)
        matrix = zeros(Float64, (n, n))
        for ((i, j), p) in zip(edge_list, edge_prop_list)
            if p.loop_length <= 6
                matrix[i, j] = 1
                matrix[j, i] = 1
            end
        end
        matrix
    end
    @info "Constructed Hamiltonian (size = $(size(hopping_hamiltonian_dense)))"

    #eigenvalues, eigenvectors = eigen(Hermitian(hopping_hamiltonian_dense))
    eigenvalues, eigenvectors = spl.eigh(hopping_hamiltonian_dense, driver="ev", overwrite_a=true)
    @info "Diagonalized"

    prefix = args["o"]

    
    output_directory = dirname(prefix)
    if !isdir(output_directory)
        mkpath(output_directory)
    end

    println("Saving eigenvalues")
    open("$prefix-eigenvalues.msgpack.xz", "w") do io
        ioc = XzCompressorStream(io)
        MsgPack.pack(ioc, eigenvalues)
        close(ioc)
    end

    println("Saving vertexnames")
    open("$prefix-vertexnames.msgpack.xz", "w") do io
        ioc = XzCompressorStream(io)
        MsgPack.pack(ioc, vertex_name_list)
        close(ioc)
    end

    println("Saving eigenvectors")
    for i in 1:size(eigenvectors, 1)
        print(i, ' ')
        open("$prefix-eigenvectors2-row-$i.msgpack.xz", "w") do io
            ioc = XzCompressorStream(io)
            MsgPack.pack(ioc, abs2.(eigenvectors[i,:]))
            close(ioc)
        end
    end
end


main()
