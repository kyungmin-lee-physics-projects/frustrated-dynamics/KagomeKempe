using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using PyCall
using PyPlot
using LightGraphs
using SimpleWeightedGraphs
using Printf
using LsqFit
using Distributions

mpl = pyimport("matplotlib")
mpl.use("Agg")

using DataFrames
using CSV

scipy = pyimport("scipy")
scipy_stats = pyimport("scipy.stats")

function loadweightednet(io::IO, gname::String = "graph", ::Type{T}=Float64) where {T}
    line = readline(io)
    # skip comments
    while startswith(line, "%")
        line = readline(io)
    end
    n = parse(Int, match(r"\d+", line).match)
    for ioline in eachline(io)
        line = ioline
        (occursin(r"^\*Arcs", line) || occursin(r"^\*Edges", line)) && break
    end
    if occursin(r"^\*Arcs", line)
        g = SimpleWeightedGraphs.SimpleWeightedDiGraph(n)
    else
        g = SimpleWeightedGraphs.SimpleWeightedGraph(n)
    end
    while occursin(r"^\*Arcs", line)
        for ioline in eachline(io)
            line = ioline
            ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 3 && break
            add_edge!(g, parse(Int, ms[1]), parse(Int, ms[2]), parse(T, ms[3]))
        end
    end
    while occursin(r"^\*Edges", line) # add edges in both directions
        for ioline in eachline(io)
            line = ioline
            ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 2 && break
            i1, i2 = parse(Int, ms[1]), parse(Int, ms[2])
            w = parse(T, ms[3])
            add_edge!(g, i1, i2, w)
            add_edge!(g, i2, i1, w)
        end
    end
    return g
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
            nargs = '+'
    end
    return parse_args(s)
end


function make_ecdf(data, weights)
    (x) -> sum(weights[data .< x])
end


function main()
    parsed_args = parse_commandline()

    fig = PyPlot.figure(figsize=(4, 3.5))
    for shape_str = parsed_args["shape"]
        @info "Plotting $shape_str"
        df = CSV.read(datadir("neighbor-histogram-6_$(shape_str).csv"), DataFrame)

        λ = sum(df[:, :degree] .* df[:, :frequency]) / sum(df[:, :frequency])

        fig.clf()
        ax = fig.gca()
        
        edges = -0.5:(maximum(df[:, :degree])+0.5)
        ax.hist(df[:, :degree], weights=df[:, :frequency], bins=edges)
        ax.set_ylim(0, nothing)
        ax.set_xlim(-0.5, nothing)
        ax.set_xlabel("degree", fontsize=16)
        ax.set_ylabel("frequency", fontsize=16)
        # leg = ax.legend(loc=1, title=shape_str)
        # ax.legend(title=(@sprintf "%s\nmean(degree) = %.2f" shape_str λ))
        ax.set_title((@sprintf "%s  mean(degree) = %.2f" shape_str λ))

        fig.savefig(plotsdir("histogram-degree-pdf-bar_$shape_str.pdf"), dpi=300, bbox_inches="tight")
        fig.savefig(plotsdir("histogram-degree-pdf-bar_$shape_str.png"), dpi=300, bbox_inches="tight")


        poisson = Poisson(λ)
        xs = 0:maximum(df[:, :degree])

        label = "\$\\lambda = $(@sprintf "%.3f" λ)\$"
        ax.plot(xs, pdf.(poisson, xs) .* sum(df[:, :frequency]), "o-", label=label)

        fig.savefig(plotsdir("histogram-degree-pdf-fit-bar_$shape_str.pdf"), dpi=300, bbox_inches="tight")
        fig.savefig(plotsdir("histogram-degree-pdf-fit-bar_$shape_str.png"), dpi=300, bbox_inches="tight")
    end
end

main()

