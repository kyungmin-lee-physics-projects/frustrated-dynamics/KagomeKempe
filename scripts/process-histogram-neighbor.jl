using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using LightGraphs
using SimpleWeightedGraphs
using StatsBase

function loadweightednet(io::IO, gname::String = "graph", ::Type{T}=Float64) where {T}
    line = readline(io)
    # skip comments
    while startswith(line, "%")
        line = readline(io)
    end
    n = parse(Int, match(r"\d+", line).match)
    for ioline in eachline(io)
        line = ioline
        (occursin(r"^\*Arcs", line) || occursin(r"^\*Edges", line)) && break
    end
    if occursin(r"^\*Arcs", line)
        g = SimpleWeightedGraphs.SimpleWeightedDiGraph(n)
    else
        g = SimpleWeightedGraphs.SimpleWeightedGraph(n)
    end
    while occursin(r"^\*Arcs", line)
        for ioline in eachline(io)
            line = ioline
            ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 3 && break
            add_edge!(g, parse(Int, ms[1]), parse(Int, ms[2]), parse(T, ms[3]))
        end
    end
    while occursin(r"^\*Edges", line) # add edges in both directions
        for ioline in eachline(io)
            line = ioline
            ms = collect(m.match for m in eachmatch(r"[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)", line, overlap=false))
            length(ms) < 2 && break
            i1, i2 = parse(Int, ms[1]), parse(Int, ms[2])
            w = parse(T, ms[3])
            add_edge!(g, i1, i2, w)
            add_edge!(g, i2, i1, w)
        end
    end
    return g
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
            nargs = '+'
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()

    for shape_str = parsed_args["shape"]
        if isfile(datadir("kempe6-$shape_str.lg"))
            @info "$shape_str: Reading data kempe6-$shape_str.lg"
            graph = loadgraph(datadir("kempe6-$shape_str.lg"))
            neighbor_count_list = degree(graph)
            neighbor_count_map = countmap(neighbor_count_list)
        else
            @info "$shape_str: Reading data kempe-$shape_str.net"
            graph = open(datadir("kempe-$shape_str.net"), "r") do io
                loadweightednet(io)
            end

            @info "$shape_str: Computing neighbors"
            graph6 = graph.weights .== 6.0
            neighbor_count_list = [sum(r) for r in eachrow(graph6)]
            neighbor_count_map = countmap(neighbor_count_list)
        end

        @info "$shape_str: Saving histogram"
        ks = sort(collect(keys(neighbor_count_map)))
        try
            tempfilepath = tempname()
            open(tempfilepath, "w") do io
                println(io, "degree,frequency")
                for k in ks
                    println(io, "$k,$(neighbor_count_map[k])")
                end
            end
            mv(tempfilepath, datadir("neighbor-histogram-6_$shape_str.csv"), force=true)
        catch e
            @error e
        end
    end
end

main()

