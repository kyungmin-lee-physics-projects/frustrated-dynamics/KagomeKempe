using DrWatson
quickactivate(joinpath(@__DIR__, ".."))

using ArgParse
using LightGraphs
using GraphPlot
using Cairo, Compose
using PyPlot

using LsqFit

using KagomeKempe
using LinearAlgebra

function read_graph(filename::AbstractString)
    open(filename, "r") do io
        return read_graph(io)
    end
end

function read_graph(io::IO)
    v, vertex_count_str = split(readline(io))
    if lowercase(v) != "*vertices"
        throw(ArgumentError("parse error at $v"))
    end
    vertex_count = parse(Int, vertex_count_str)

    vertex_list = Vector{String}(undef, vertex_count)

    for i in 1:vertex_count
        vertex_index_str, vertex_name = split(readline(io))
        if i != parse(Int, vertex_index_str)
            throw(ArgumentError("parse error at $vertex_index_str"))
        end
        vertex_list[i] = vertex_name
    end

    line = readline(io)
    if lowercase(split(line)[1]) != "*edges"
        throw(ArgumentError("parse error at $line"))
    end

    edge_list = Tuple{Int, Int, Int}[]
    while !eof(io)
        line = readline(io)
        if isempty(strip(line))
            break
        end
        (i, j, k) = let
            i, j, k = split(line)
            (parse(Int, i), parse(Int, j), parse(Int, k))
        end
        if !(1 <= i <= vertex_count && 1 <= j <= vertex_count)
            throw(ArgumentError("vertex out of bounds"))
        end
        push!(edge_list, (i,j,k))
    end

    return vertex_list, edge_list
end


function find_components(vertex_list, edge_list)

    loop_length_list = sort(unique(x[3] for x in edge_list))
    #@show loop_length_list


    # at loop length 0, every vertex is a component
    components = Dict(0 => (
        #component_count=length(vertex_list),
        component_list=[[x] for x in 1:length(vertex_list)],
        membership=collect(1:length(vertex_list)),
    ))

    prev_loop_length = 0
    for curr_loop_length in loop_length_list

        # components of previous are vertices of current
        prev_component_list = components[prev_loop_length].component_list
        prev_membership = components[prev_loop_length].membership

        curr_vertex_count = length(prev_component_list)

        # ---
        curr_graph = SimpleGraph(curr_vertex_count)
        for (i, j, k) in edge_list
            if k == curr_loop_length
                ic = prev_membership[i]
                jc = prev_membership[j]
                add_edge!(curr_graph, (ic, jc))
            end
        end
        compo_list = connected_components(curr_graph)
        # ---

        curr_membership = Vector{Int}(undef, length(vertex_list))
        fill!(curr_membership, -1)

        #for i in eachindex(vertex_list)
        for (icompo, compo) in enumerate(compo_list)
            for ic in compo
                for i in prev_component_list[ic]
                    curr_membership[i] = icompo
                end
            end
        end

        curr_component_list = [Int[] for i in compo_list]
        for (i, icompo) in enumerate(curr_membership)
            push!(curr_component_list[icompo], i)
        end

        components[curr_loop_length] = (
            component_list=curr_component_list,
            membership=curr_membership,
        )
        prev_loop_length = curr_loop_length
    end
    return components
end



function make_hierarch(components)

    all_node_list = Tuple{Int, Int}[]
    for loop_length in sort(collect(keys(components)))
        for i in 1:length(components[loop_length].component_list)
            push!(all_node_list, (loop_length, i))
        end
    end
    all_node_lookup = Dict(x => i for (i, x) in enumerate(all_node_list))
    #@show all_node_lookup

    #graph = SimpleDiGraph(length(all_node_list))
    all_edge_list = Tuple{Int, Int}[]

    prev_loop_length = 0
    prev = components[0]

    loop_length_list = sort(collect(keys(components)))
    for curr_loop_length in loop_length_list[2:end]

        curr = components[curr_loop_length]

        curr.component_list
        curr.membership

        # prev is leafy

        for (idx_prev_compo, prev_compo) in enumerate(prev.component_list)
            i = prev_compo[1]
            idx_curr_compo = curr.membership[i]

            ic = all_node_lookup[(curr_loop_length, idx_curr_compo)]
            ip = all_node_lookup[(prev_loop_length, idx_prev_compo)]
            #add_edge!(graph, (ic, ip))
            push!(all_edge_list, (ic, ip))
        end

        prev_loop_length = curr_loop_length
        prev = curr
    end
    return all_node_list, all_edge_list
end

function parse_commandline()
    s = ArgParse.ArgParseSettings()
    @add_arg_table! s begin
        "shape"
            arg_type = String
            required = true
            nargs = '+'
    end
    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    fig = PyPlot.figure()
    ax = fig.gca()

    size_vs_coloring = Dict(:x=>Float64[], :y=>Float64[])

    for shape_str in parsed_args["shape"]
        shape = KagomeKempe.parse_shape(shape_str)
        vertex_list, edge_list = read_graph(datadir("kempe-$shape_str.net"))

        n_sites = round(Int, 3 * det(shape))
        push!(size_vs_coloring[:x], n_sites)
        push!(size_vs_coloring[:y], length(vertex_list))

        components = find_components(vertex_list, edge_list)

        node_list, edge_list = make_hierarch(components)

        # @show node_list
        # @show edge_list

        graph = SimpleDiGraph(length(node_list))
        for (i, j) in edge_list
            add_edge!(graph, (i, j))
        end
        # @show graph

        length_map = Dict{Int, Vector{Int}}()
        for (i, (l, _)) in enumerate(node_list)
            if !haskey(length_map, l)
                length_map[l] = Int[]
            end
            push!(length_map[l], i)
        end
        # @show length_map

        length_list = sort(collect(keys(length_map)))

        count = Vector{Int}(undef, length(node_list))
        fill!(count, -4*length(node_list))

        # leaf nodes
        for (i, n) in enumerate(node_list)
            if n[1] == 0
                count[i] = 1
            end
        end

        # nonleaf
        for l in length_list
            if l == 0
                continue
            end
            for i in length_map[l]
                # @show l, i, neighbors(graph, i)
                count[i] = sum(count[neighbors(graph, i)])
            end
        end

        # length vs size distro
        length_size_map = Dict{Int, Vector{Int}}()
        for (k, v) in length_map
            length_size_map[k] = [count[x] for x in v]
        end
        # @show length_size_map

        let
            x = Float64[]
            y = Float64[]
            for (l, v) in length_size_map
                push!(x, l)
                push!(y, sum(v) / length(v))
                #push!(y, sum(inv.(v)) / length(v))
                #push!(y, sum(v.^1.13) / sum(v))
                #for c in v
                #    push!(x, l)
                #    push!(y, c)
                #end
            end
            let
                idx = sortperm(x)
                x[:] = x[idx]
                y[:] = y[idx]
            end
            #ax.hist2d(x, y, bins=100)
            #ax.scatter(x.+1,y)
            ax.plot(x, y, "o", label="$n_sites", alpha=0.5)
        end
    end

    #=
    let
        idx = sortperm(size_vs_coloring[:x])
        size_vs_coloring[:x] = size_vs_coloring[:x][idx]
        size_vs_coloring[:y] = size_vs_coloring[:y][idx]
    end

    fitfunc = let
        @. model(x, p) = p[1] + p[2]*x
        x = size_vs_coloring[:x]
        y = size_vs_coloring[:y]
        fit = curve_fit(model, x, log.(y), [1.0, 1.0])
        x -> exp(model(x, fit.param))
    end

    fig.clf()
    ax = fig.gca()

    ax.plot(size_vs_coloring[:x], size_vs_coloring[:y], "o-")

    xfit = minimum(size_vs_coloring[:x]):0.1:maximum(size_vs_coloring[:x])
    yfit = fitfunc.(xfit)
    ax.plot(xfit, yfit)

    #ax.set_xscale("log", basex=10)
    =#
    ax.legend()
    ax.set_yscale("log", basey=10)

    #ax.set_ylabel("component size")
    ax.set_xlabel("loop length")
    fig.savefig("foo.png", dpi=300)
end


main()
